﻿using RobinBird.FirebaseTools.Storage.Addressables;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class TextureLoadTester : MonoBehaviour
{
    [SerializeField]
    private AssetReferenceSprite spriteReference;

    [SerializeField]
    private SpriteRenderer spriteRenderer;
    
    void Awake()
    {
        FirebaseAddressablesManager.FirebaseSetupFinished += () =>
        {
            spriteReference.LoadAssetAsync().Completed += handle => { spriteRenderer.sprite = handle.Result; };
        };
    }
}
