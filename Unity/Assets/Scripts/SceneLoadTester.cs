﻿using RobinBird.FirebaseTools.Storage.Addressables;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Tests loading scenes and making sure they clean up correctly when being unloaded
/// </summary>
public class SceneLoadTester : MonoBehaviour
{
    [SerializeField]
    private AssetReference sceneReference;

    [SerializeField]
    private Button sceneLoadButton;

    [SerializeField]
    private Text sceneLoadText;

    [SerializeField]
    private SceneInstance sceneInstance;
    
    void Awake()
    {
        FirebaseAddressablesManager.FirebaseSetupFinished += LoadScene;
    }

    private void LoadScene()
    {
        Addressables.LoadSceneAsync(sceneReference, LoadSceneMode.Additive).Completed += handle =>
        {
            sceneInstance = handle.Result;
            SetupUnloadButton();
            Debug.Log("Scene loaded successfully");
        };
    }

    private void SetupLoadButton()
    {
        sceneLoadButton.onClick.RemoveAllListeners();
        sceneLoadButton.onClick.AddListener(LoadScene);
        sceneLoadText.text = $"Load {sceneReference.RuntimeKey}";
    }

    private void UnloadScene()
    {
        if (sceneInstance.Scene.IsValid())
        {
            Addressables.UnloadSceneAsync(sceneInstance).Completed += handle =>
            {
                SetupLoadButton();
                Debug.Log("Scene unloaded successfully");
            };
        }
    }

    private void SetupUnloadButton()
    {
        sceneLoadButton.onClick.RemoveAllListeners();
        sceneLoadButton.onClick.AddListener(UnloadScene);
        sceneLoadText.text = $"Unload {sceneReference.RuntimeKey}";
    }
}
